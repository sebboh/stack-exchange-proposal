StackExchange has a culture.  Meta.SO has its own culture.  This is complex and I know more about the former than the latter.

Over ten years ago, [SE announced that they would donate advertising space to open source projects](https://stackoverflow.blog/2009/12/19/free-vote-based-advertising-for-open-source-projects/) with the intent to help those projects recruit new contributers.

Volunteers from the SE userbase make the ads and then the users vote on which ads should go live, then they go live.

I have previously submitted an ad for the FSF and it was rejected.  The reason it was rejected was that the FSF itself is not a software project.  That's what the comments that the downvoters left said.

So, this year, I'm asking them to amend their policy to allow organizations such as the FSF to participate in the `open-source-advertisment` program.

My proposal has received one upvote and six downvotes so far.  (That's bad.)  Here is [my proposal](https://meta.stackoverflow.com/questions/392602/should-we-amend-the-open-source-advertising-policy-to-allow-ads-from-organizatio).  It is online now, but it may be deleted for getting too many downvotes at any minute.

There are [statistics available publically](https://meta.stackoverflow.com/questions/367520/why-is-the-click-rate-way-down-on-open-source-advertisements) that describe how many eyeballs get exposed to SE ads and there are specific line items about the "open-source" ads.  As you can see, my link doesn't lead to all such statistics.. I don't know where that user got those links from.

How do I improve my proposal?