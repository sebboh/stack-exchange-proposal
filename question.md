Wow, it is 2020, already.  I'm sure the mods will soon make a post with the [`community-ads`](https://meta.stackoverflow.com/questions/tagged/community-ads) tag.

Here is a snippet of 2H 2019's policy regarding what kind of ads can qualify (the emphasis is in the original):

> It must be an advertisement **soliciting the participation and contribution of programmers writing actual source code**. This is not intended as a general purpose ad for consumer products which just happen to be open source. It's for finding programmers who will help contribute code or other programmery things (documentation, code review, bug fixes, etc.).

I propose amending it to something like this:

> It must be an advertisement **soliciting the participation and contribution of programmers writing actual source code or soliciting membership in organizations that themselves encourage such participation and contribution**. This is not intended as a general purpose ad for consumer products which just happen to be open source. It's for finding programmers who will help contribute code or other programmery things (documentation, code review, bug fixes, etc.).

Here are some examples of organizations that would become included:

* [The Free Software Foundation](https://www.fsf.org/)
* [The Free Software Foundation Europe](https://fsfe.org/)
* [The Free Software Foundation France](http://www.fsffrance.org/index.en.html)
* [The Free Software Foundation India](http://fsf.org.in/)
* [The Free Software Foundation Latin America](http://www.fsfla.org/)
* [Software Freedom Conservancy](https://sfconservancy.org/)

*Note that all those Foundations are independent organizations.  They are not branches of one organization.*